package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"time"

	"github.com/fatih/color"
	"golang.org/x/crypto/ssh"
)

func readPrivateKey(privateKeyPath, passphrase string) (*ssh.Signer, error) {
	pemBytes, err := ioutil.ReadFile(privateKeyPath)
	if err != nil {
		return nil, err
	}
	var key ssh.Signer
	if passphrase != "" {
		key, err = ssh.ParsePrivateKeyWithPassphrase(pemBytes, []byte(passphrase))
		if err != nil {
			return nil, err
		}
	} else {
		key, err = ssh.ParsePrivateKey(pemBytes)
		if err != nil {
			return nil, err
		}
	}
	return &key, nil
}

func readFileFromRemoteHost(host, port, filePath string, config *ssh.ClientConfig) ([]byte, error) {
	client, err := ssh.Dial("tcp", fmt.Sprintf("%s:%s", host, port), config)
	if err != nil {
		return nil, err
	}
	session, err := client.NewSession()
	if err != nil {
		return nil, err
	}
	defer session.Close()
	return session.Output(fmt.Sprintf("cat %s", filePath))
}

type tagWriter struct {
	tag    string
	writer io.Writer
}

func (t *tagWriter) Write(p []byte) (n int, err error) {
	tag := []byte(t.tag)
	n1, err := t.writer.Write(append(tag, p...))
	if err != nil {
		return n1 - len(tag), err
	}
	return n1 - len(tag), nil
}

func doWithRetry(fn func() error, retry int) error {
	attempt := 1
	for {
		err := fn()
		if err == nil {
			return nil
		}
		attempt++
		if attempt > retry {
			return err
		}
		time.Sleep(time.Duration(1*attempt) * time.Second)
	}
}

// ok i think this should work
func connectToHost(fn func() (*ssh.Client, error), retry int) (*ssh.Client, error) {
	var client *ssh.Client
	var err error
	err = doWithRetry(func() error {
		client, err = fn()
		return err
	}, retry)
	return client, err
}

func runCmdOnRemoteHost(host, port, cmd string, config *ssh.ClientConfig) error {
	client, err := connectToHost(func() (*ssh.Client, error) { return ssh.Dial("tcp", fmt.Sprintf("%s:%s", host, port), config) }, 5)
	if err != nil {
		return err
	}
	session, err := client.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()

	stdout := &tagWriter{tag: color.HiCyanString(fmt.Sprintf("%s:\t", host)), writer: os.Stdout}
	session.Stdout = stdout
	stderr := &tagWriter{tag: color.HiRedString(fmt.Sprintf("%s:\t", host)), writer: os.Stderr}
	session.Stderr = stderr

	if err := session.Run(cmd); err != nil {
		return fmt.Errorf("Failed to run '%s': %s,", cmd, err)
	}

	return nil
}
