#!/bin/bash
set -e
# disable swap cause kubernetes doesn't like it?
swapoff -a
sed -ri '/\sswap\s/s/^#?/#/' /etc/fstab

curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

release="$(lsb_release -cs)";
# wanted to use ubuntu 18.04 but it doesn't have a release file, so we just 16.04 one
# and cross our fingers
# add-apt-repository "deb https://apt.kubernetes.io/ kubernetes-${release} main"
add-apt-repository --no-update "deb https://apt.kubernetes.io/ kubernetes-xenial main" && \
add-apt-repository --no-update "deb [arch=amd64] https://download.docker.com/linux/ubuntu ${release} stable";

k8sVersion="1.16.4";
dockerVersion="18.06.2~ce~3-0~ubuntu";
apt-get -qq update && apt-get -y -qq install --allow-downgrades docker-ce=${dockerVersion} \
    kubelet=${k8sVersion}-00 kubeadm=${k8sVersion}-00 kubectl=${k8sVersion}-00;

cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
systemctl daemon-reload
systemctl restart docker