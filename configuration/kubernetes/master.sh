#!/bin/bash
set -e

apiserveraddr=
while getopts "a:" opt; do 
	case $opt in
		a)
			apiserveraddr=$OPTARG
		;;
	esac
done

# init
if [ ! -z $apiserveraddr ]; then
    kubeadm init --ignore-preflight-errors=NumCPU --apiserver-advertise-address $apiserveraddr
else
    kubeadm init --ignore-preflight-errors=NumCPU
fi

kubeadm token create --print-join-command > /tmp/join_command

mkdir -p /home/ubuntu/.kube
cp /etc/kubernetes/admin.conf /home/ubuntu/.kube/config
chown -R ubuntu /home/ubuntu/.kube

kubectl create -f https://raw.githubusercontent.com/cilium/cilium/1.6.5/install/kubernetes/quick-install.yaml
kubectl create -f https://raw.githubusercontent.com/rook/rook/release-1.0/cluster/examples/kubernetes/ceph/common.yaml
kubectl apply -f https://gitlab.com/freddieptf/lolwhat/-/raw/master/configuration/kubernetes/storage/ceph_cluster.yml
kubectl apply -f https://gitlab.com/freddieptf/lolwhat/-/raw/master/configuration/kubernetes/ingress/nginx_ingress_controller.yml