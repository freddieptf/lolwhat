#!/bin/bash
set -e
groupadd nopasssudo
printf "\n%%nopasssudo ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
useradd -m -d /home/ubuntu -s /bin/bash ubuntu
usermod -aG nopasssudo ubuntu
rsync --archive --chown=ubuntu:ubuntu ~/.ssh /home/ubuntu