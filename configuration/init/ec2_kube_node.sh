#!/bin/bash
set -e
public_ip=$(curl -s http://169.254.169.254/latest/meta-data/public-ipv4)
hostname $public_ip
ifconfig eth0:1 $public_ip netmask 255.255.255.0 up
systemctl restart kubelet