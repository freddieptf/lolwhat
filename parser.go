package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	hcl_jsonparser "github.com/hashicorp/hcl/json/parser"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclparse"
)

const (
	AWS_PROVIDER           = "aws"
	DIGITAL_OCEAN_PROVIDER = "digitalocean"
)

type VM struct {
	Provider string  `hcl:"provider"`
	Role     string  `hcl:"role"`
	Image    *string `hcl:"image"`
	Size     *string `hcl:"size"`
	Count    *int    `hcl:"count"`
	Name     string  `hcl:"identifier,label"`
	Region   *string `hcl:"region"`
	Tags     map[string]string
}

type connection struct {
	Type string `json:"type"`
	User string `json:"user"`
	Host string `json:"host"`
}

type remoteExec struct {
	InlineCmds []string   `json:"inline"`
	Connection connection `json:"connection"`
}

type DigitalOceanDroplet struct {
	Image             string                 `json:"image"`
	Count             *int                   `json:"count,omitempty"`
	Name              string                 `json:"name"`
	Region            string                 `json:"region"`
	Size              string                 `json:"size"`
	PrivateNetworking *bool                  `json:"private_networking,omitempty"`
	SshKeys           []string               `json:"ssh_keys"`
	Provisioners      map[string]interface{} `json:"provisioner,omitempty"`
	Tags              *[]string              `json:"tags,omitempty"`
}

type DropletResourceWrapper struct {
	Body map[string]DigitalOceanDroplet `json:"digitalocean_droplet"`
}

type Ec2Instance struct {
	Image            string   `json:"ami"`
	Count            *int     `json:"count,omitempty"`
	AZ               *string  `json:"availability_zone,omitempty"`
	InstanceType     string   `json:"instance_type"`
	SSHKeyName       string   `json:"key_name"`
	SubnetID         string   `json:"subnet_id"`
	SecurityGroupIDs []string `json:"vpc_security_group_ids"`
	BlockDevice      struct {
		VolumeSize int    `json:"volume_size"`
		VolumeType string `json:"volume_type"`
	} `json:"root_block_device"`
	Tags map[string]string `json:"tags"`
}

type Ec2InstanceResourceWrapper struct {
	Body map[string]Ec2Instance `json:"aws_instance"`
}

func setVMDefaults(provider string, defaults map[string]string, vm *VM) {
	if defaults["vm_image"] != "" && vm.Image == nil {
		image := defaults["vm_image"]
		vm.Image = &image
	}
	if defaults["vm_size"] != "" && vm.Size == nil {
		size := defaults["vm_size"]
		vm.Size = &size
	}
	if defaults["region"] != "" && vm.Region == nil && provider != AWS_PROVIDER { // yeah fail fast for any new providers
		region := defaults["region"]
		vm.Region = &region
	}
}

func getProviderVMConfig(defaults map[string]string, publicKeyName string, vm VM) (map[string]interface{}, error) {
	setVMDefaults(vm.Provider, defaults, &vm)
	switch vm.Provider {
	case DIGITAL_OCEAN_PROVIDER:
		vmName := ""
		if vm.Count != nil {
			vmName = vm.Name + "${count.index}"
		} else {
			vmName = vm.Name
		}
		doVM := DigitalOceanDroplet{
			Name:    vmName,
			Image:   *vm.Image,
			Region:  *vm.Region,
			Size:    *vm.Size,
			Count:   vm.Count,
			SshKeys: []string{fmt.Sprintf("${digitalocean_ssh_key.%s.fingerprint}", publicKeyName)},
			Tags:    &[]string{vm.Name, fmt.Sprintf("Role:%s", vm.Role)}, // hopefully name will always be at position 0
		}
		doVM.Provisioners = map[string]interface{}{
			"remote-exec": remoteExec{
				InlineCmds: []string{
					"curl -s -o /tmp/init.sh https://gitlab.com/freddieptf/lolwhat/raw/a7b12b4680250a41d97a701dacc48e32cbcb552c/configuration/init/digitalocean_droplet.sh",
					"chmod +x /tmp/init.sh",
					"/tmp/init.sh",
				},
				Connection: connection{
					Type: "ssh",
					User: "root",
					Host: "${self.ipv4_address}",
				},
			},
		}
		configMap := make(map[string]interface{})
		configMap["resource"] = DropletResourceWrapper{
			Body: map[string]DigitalOceanDroplet{
				vm.Name: doVM,
			},
		}
		return configMap, nil

	case AWS_PROVIDER:
		tags := make(map[string]string)
		if vm.Count != nil {
			tags["Name"] = fmt.Sprintf("%s-${count.index}", vm.Name)
		} else {
			tags["Name"] = vm.Name
		}
		tags["Role"] = vm.Role
		ec2Instance := Ec2Instance{
			Image:        *vm.Image,
			InstanceType: *vm.Size,
			Count:        vm.Count,
			Tags:         tags,
			BlockDevice: struct {
				VolumeSize int    `json:"volume_size"`
				VolumeType string `json:"volume_type"`
			}{50, "gp2"},
			SubnetID:         "${module.aws_vpc.public_subnets[0]}",
			SecurityGroupIDs: []string{"${module.aws_firewall.this_security_group_id}", "${module.aws_firewall_ssh.this_security_group_id}"},
			SSHKeyName:       fmt.Sprintf("${aws_key_pair.%s.key_name}", publicKeyName),
		}
		if vm.Role == "master" {
			ec2Instance.SecurityGroupIDs = append(ec2Instance.SecurityGroupIDs, "${module.aws_firewall_http.this_security_group_id}")
		}
		configMap := make(map[string]interface{})
		configMap["resource"] = Ec2InstanceResourceWrapper{
			Body: map[string]Ec2Instance{
				vm.Name: ec2Instance,
			},
		}
		return configMap, nil

	}
	return nil, nil
}

func addTerraformProviderConfigs(defaults map[string]map[string]string, configs map[string][]map[string]interface{}) map[string][]map[string]interface{} {
	for provider := range configs {
		if provider == AWS_PROVIDER {
			configs[provider] = append(configs[provider],
				map[string]interface{}{
					"provider": map[string]struct {
						Region string `json:"region"`
					}{
						provider: struct {
							Region string `json:"region"`
						}{Region: defaults[provider]["region"]},
					},
				},
			)
		} else {
			configs[provider] = append(configs[provider],
				map[string]interface{}{
					"provider": map[string]struct{}{
						provider: struct{}{},
					},
				})
		}
	}
	return configs
}

func doesSSHResourceExist(provider string, resources []map[string]interface{}) bool {
	var keyResourceExists = false
	for _, resource := range resources {
		switch provider {
		case DIGITAL_OCEAN_PROVIDER:
			_, keyResourceExists = resource["digitalocean_ssh_key"]
			if keyResourceExists {
				break
			}
		case AWS_PROVIDER:
			_, keyResourceExists = resource["aws_key_pair"]
			if keyResourceExists {
				break
			}
		}
	}
	return keyResourceExists
}

type publicKey struct {
	Name    string `hcl:"name"`
	KeyPath string `hcl:"path"`
}

type DigitalOceanSSHKeyResource struct {
	Name string `json:"name"`
	Key  string `json:"public_key"`
}

type AWSSSHKeyResource struct {
	Name string `json:"key_name"`
	Key  string `json:"public_key"`
}

type DigitalOceanSSHResourceWrapper struct {
	Body map[string]DigitalOceanSSHKeyResource `json:"digitalocean_ssh_key"`
}

type AWSSSHResourceWrapper struct {
	Body map[string]AWSSSHKeyResource `json:"aws_key_pair"`
}

func addSSHKeyResources(key publicKey, configs map[string][]map[string]interface{}) map[string][]map[string]interface{} {
	for provider, providerConfs := range configs {
		if !doesSSHResourceExist(provider, providerConfs) {
			keyConfig := make(map[string]interface{})
			switch provider {
			case DIGITAL_OCEAN_PROVIDER:
				doKey := DigitalOceanSSHKeyResource{Name: key.Name}
				doKey.Key = fmt.Sprintf("${file(\"%s\")}", key.KeyPath)
				keyConfig["resource"] = DigitalOceanSSHResourceWrapper{
					Body: map[string]DigitalOceanSSHKeyResource{
						key.Name: doKey,
					},
				}

			case AWS_PROVIDER:
				awsKey := AWSSSHKeyResource{Name: key.Name}
				awsKey.Key = fmt.Sprintf("${file(\"%s\")}", key.KeyPath)
				keyConfig["resource"] = AWSSSHResourceWrapper{
					Body: map[string]AWSSSHKeyResource{
						key.Name: awsKey,
					},
				}
			}
			configs[provider] = append(configs[provider], keyConfig)
		}
	}

	return configs
}

type DigitalOceanFirewall struct {
	Source            string `json:"source"`
	DropletIDs        string `json:"droplet_ids"`
	NodeIPs           string `json:"node_public_ips"`
	HasPublicDroplets bool   `json:"has_public_droplets"`
	PublicDropletIDs  string `json:"public_droplet_ids"`
}

type DigitalOceanResourceFirewallWrapper struct {
	FirewallModule DigitalOceanFirewall `json:"firewall"`
}

type AwsVPC struct {
	Source     string   `json:"source"`
	Version    string   `json:"version"`
	VpcCIDR    string   `json:"cidr"`
	SubnetCIDR []string `json:"public_subnets"`
	AZs        []string `json:"azs"`
}

type AwsVPCResourceWrapper struct {
	VPCModule AwsVPC `json:"aws_vpc"`
}

type AwsFirewall struct {
	Name         string    `json:"name"`
	Source       string    `json:"source"`
	Version      string    `json:"version"`
	VPCID        string    `json:"vpc_id"`
	IngressCIDR  string    `json:"ingress_cidr_blocks"`
	IngressRules *[]string `json:"ingress_rules,omitempty"`
	EgressRules  *[]string `json:"egress_rules,omitempty"`
}

func getVMResources(providers map[string][]map[string]interface{}) []interface{} {
	vms := []interface{}{}
	for _, confs := range providers {
		for _, resource := range confs {
			for _, v := range resource {
				switch v.(type) {
				case DropletResourceWrapper:
					{
						vms = append(vms, v)
					}
				case Ec2InstanceResourceWrapper:
					{
						vms = append(vms, v)
					}
				}
			}
		}
	}
	return vms
}

func firewall(defaults map[string]map[string]string, providers map[string][]map[string]interface{}) map[string][]map[string]interface{} {
	vms := getVMResources(providers)
	for provider := range providers {
		switch provider {
		case DIGITAL_OCEAN_PROVIDER:
			var dropletIDs string
			var nodeIPs string
			publicDropletID := ""
			for _, v := range vms {
				switch v.(type) {
				case DropletResourceWrapper:
					{
						dropletResource := v.(DropletResourceWrapper)
						for name, droplet := range dropletResource.Body {
							dropletIDs += fmt.Sprintf("digitalocean_droplet.%s.*.id,", name)
							nodeIPs += fmt.Sprintf("digitalocean_droplet.%s.*.ipv4_address,", name)
							for _, tag := range *droplet.Tags {
								if strings.Contains("Role:master", tag) {
									publicDropletID = fmt.Sprintf("digitalocean_droplet.%s.*.id", name)
								}
							}
						}
					}
				case Ec2InstanceResourceWrapper:
					{
						ec2InstanceResource := v.(Ec2InstanceResourceWrapper)
						for name := range ec2InstanceResource.Body {
							nodeIPs += fmt.Sprintf("aws_instance.%s.*.public_ip,", name)
						}
					}
				}
			}
			firewall := make(map[string]interface{})
			firewall["module"] = DigitalOceanResourceFirewallWrapper{
				FirewallModule: DigitalOceanFirewall{
					Source:            "./modules/do-firewall",
					DropletIDs:        fmt.Sprintf("${concat(%s)}", dropletIDs[:len(dropletIDs)-1]),
					NodeIPs:           fmt.Sprintf("${concat(%s)}", nodeIPs[:len(nodeIPs)-1]),
					HasPublicDroplets: publicDropletID != "",
					PublicDropletIDs:  fmt.Sprintf("${%s}", publicDropletID),
				},
			}

			providers[provider] = append(providers[provider], firewall)

		case AWS_PROVIDER:
			vpc := make(map[string]interface{})
			vpc["module"] = AwsVPCResourceWrapper{
				VPCModule: AwsVPC{
					Source:     "terraform-aws-modules/vpc/aws",
					Version:    "2.21.0",
					VpcCIDR:    "172.40.0.0/20",
					SubnetCIDR: []string{"172.40.7.0/24"},
					AZs:        []string{fmt.Sprintf("%sa", defaults[provider]["region"])},
				},
			}

			providers[provider] = append(providers[provider], vpc)

			var nodePublicIPs string
			for _, v := range vms {
				switch v.(type) {
				case DropletResourceWrapper:
					{
						dropletResource := v.(DropletResourceWrapper)
						for name := range dropletResource.Body {
							nodePublicIPs += fmt.Sprintf("digitalocean_droplet.%s.*.ipv4_address,", name)
						}
					}
				case Ec2InstanceResourceWrapper:
					{
						ec2InstanceResource := v.(Ec2InstanceResourceWrapper)
						for name, instance := range ec2InstanceResource.Body {
							nodePublicIPs += fmt.Sprintf("aws_instance.%s.*.public_ip,", name)
							if role, ok := instance.Tags["Role"]; ok {
								if role == "master" {
									providers[provider] = append(providers[provider],
										map[string]interface{}{
											"module": struct {
												FirewallModule AwsFirewall `json:"aws_firewall_http"`
											}{
												FirewallModule: AwsFirewall{
													Name:         "gotta_get_that_traffic",
													Source:       "terraform-aws-modules/security-group/aws",
													Version:      "3.3.0",
													VPCID:        "${module.aws_vpc.vpc_id}",
													IngressCIDR:  "${list(\"0.0.0.0/0\")}",
													IngressRules: &[]string{"http-80-tcp", "https-443-tcp"},
												},
											}})
								}
							}
						}
					}
				}
			}

			providers[provider] = append(providers[provider],
				map[string]interface{}{
					"module": struct {
						FirewallModule AwsFirewall `json:"aws_firewall"`
					}{
						FirewallModule: AwsFirewall{
							Name:         "thems_the_rules",
							Source:       "terraform-aws-modules/security-group/aws",
							Version:      "3.3.0",
							VPCID:        "${module.aws_vpc.vpc_id}",
							IngressCIDR:  fmt.Sprintf("${concat(formatlist(\"%%s/32\", concat(%s)), [\"172.40.7.0/24\"])}", nodePublicIPs[:len(nodePublicIPs)-1]),
							IngressRules: &[]string{"all-all"},
							EgressRules:  &[]string{"all-all"},
						},
					},
				},
				map[string]interface{}{
					"module": struct {
						FirewallModule AwsFirewall `json:"aws_firewall_ssh"`
					}{
						FirewallModule: AwsFirewall{
							Name:         "ssh",
							Source:       "terraform-aws-modules/security-group/aws",
							Version:      "3.3.0",
							VPCID:        "${module.aws_vpc.vpc_id}",
							IngressCIDR:  "${list(\"0.0.0.0/0\")}",
							IngressRules: &[]string{"ssh-tcp"},
						},
					},
				})

		}
	}
	return providers
}

func setOutputs(configs map[string][]map[string]interface{}) map[string][]map[string]interface{} {
	hosts := make(map[string][]string)

	for _, v := range getVMResources(configs) {
		switch v.(type) {
		case DropletResourceWrapper:
			{
				dropletResource := v.(DropletResourceWrapper)
				if hosts[DIGITAL_OCEAN_PROVIDER] == nil {
					hosts[DIGITAL_OCEAN_PROVIDER] = []string{}
				}
				for name := range dropletResource.Body {
					publicIps := fmt.Sprintf("digitalocean_droplet.%s.*.ipv4_address", name)
					configs[DIGITAL_OCEAN_PROVIDER] = append(configs[DIGITAL_OCEAN_PROVIDER],
						map[string]interface{}{
							"output": map[string]interface{}{
								name: map[string]interface{}{
									"value": fmt.Sprintf("${%s}", publicIps),
								},
							},
						})
					hosts[DIGITAL_OCEAN_PROVIDER] = append(hosts[DIGITAL_OCEAN_PROVIDER], publicIps)
				}
			}
		case Ec2InstanceResourceWrapper:
			{
				ec2InstanceResource := v.(Ec2InstanceResourceWrapper)
				if hosts[AWS_PROVIDER] == nil {
					hosts[AWS_PROVIDER] = []string{}
				}
				for name := range ec2InstanceResource.Body {
					publicIps := fmt.Sprintf("aws_instance.%s.*.public_ip", name)
					configs[AWS_PROVIDER] = append(configs[AWS_PROVIDER],
						map[string]interface{}{
							"output": map[string]interface{}{
								name: map[string]interface{}{
									"value": fmt.Sprintf("${%s}", publicIps),
								},
							},
						})
					hosts[AWS_PROVIDER] = append(hosts[AWS_PROVIDER], publicIps)
				}
			}
		}
	}

	for provider, ips := range hosts {
		concatIps := ""
		for i, ip := range ips {
			if i < len(ips)-1 {
				concatIps += fmt.Sprintf("%s,", ip)
			} else {
				concatIps += fmt.Sprintf("%s", ip)
			}
		}
		configs[provider] = append(configs[provider],
			map[string]interface{}{
				"output": map[string]interface{}{
					provider + "_hosts": map[string]interface{}{
						"value": fmt.Sprintf("${concat(%s)}", concatIps),
					},
				},
			})
	}
	return configs
}

type Config struct {
	PublicKey *publicKey `hcl:"public_key,block"`
	Defaults  []struct {
		Identifier string `hcl:"name,label"`
		VMSize     string `hcl:"vm_size"`
		VMImage    string `hcl:"vm_image"`
		Region     string `hcl:"region"`
	} `hcl:"provider_defaults,block"`
	Nodes []VM `hcl:"node,block"`
}

func readconfig(filePath string) map[string][]map[string]interface{} {
	file, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.Fatal(err)
	}
	parser := hclparse.NewParser()
	p, r := parser.ParseHCL(file, filePath)
	if r.HasErrors() {
		log.Fatalf("%+v", r.Errs())
	}
	config := Config{}
	r = gohcl.DecodeBody(p.Body, nil, &config)
	if r.HasErrors() {
		log.Fatalf("%+v", r.Errs())
	}
	configs := make(map[string][]map[string]interface{})
	providerDefaults := make(map[string]map[string]string)

	key := publicKey{
		Name:    fmt.Sprintf("%s-public-key-lol-what", os.Getenv("USER")),
		KeyPath: filepath.Join(os.Getenv("HOME"), ".ssh", "id_rsa.pub"),
	}
	if config.PublicKey != nil {
		key = *config.PublicKey
	}

	for _, defaultC := range config.Defaults {
		providerDefaults[defaultC.Identifier] = map[string]string{
			"vm_image": defaultC.VMImage,
			"vm_size":  defaultC.VMSize,
			"region":   defaultC.Region,
		}
	}

	for _, node := range config.Nodes {
		vmConfig, err := getProviderVMConfig(providerDefaults[node.Provider], key.Name, node)
		if err != nil {
			log.Fatal(err)
		}
		configs[node.Provider] = append(configs[node.Provider], vmConfig)
	}

	// the below calls are all dependent on having providers already set from the nodes
	configs = addSSHKeyResources(key, configs)
	configs = firewall(providerDefaults, configs)
	configs = addTerraformProviderConfigs(providerDefaults, configs)
	configs = setOutputs(configs)
	return configs
}

func genHCL(configFilePath string, outDir string) error {
	hcls := readconfig(configFilePath)
	for provider, resources := range hcls {
		file, err := os.Create(filepath.Join(outDir, provider+".tf"))
		if err != nil {
			return err
		}
		for _, resource := range resources {
			hcljsonconf, _ := json.MarshalIndent(resource, "", " ")
			asts, err := hcl_jsonparser.Parse(hcljsonconf)
			if err != nil {
				return err
			}
			hclconf, err := hclPrint(asts)
			if err != nil {
				return err
			}
			fmt.Fprintf(file, "%s\n", hclconf)
		}
	}
	return nil
}
