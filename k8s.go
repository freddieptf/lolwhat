package main

import (
	"fmt"
	"log"
	"sync"
	"time"

	"golang.org/x/crypto/ssh"
)

func initNodes(multiCloud bool, nodes []node, config *ssh.ClientConfig) {
	var wg sync.WaitGroup
	for _, node := range nodes {
		wg.Add(1)
		go func(provider, host, port string, config *ssh.ClientConfig) {
			defer wg.Done()
			cmd := fmt.Sprintf("%s && %s && %s",
				"curl -s -o ./node.sh https://gitlab.com/freddieptf/lolwhat/raw/master/configuration/kubernetes/node.sh",
				"chmod +x ./node.sh", "sudo ./node.sh")
			if multiCloud && provider == AWS_PROVIDER {
				cmd = fmt.Sprintf("%s && %s && %s && %s",
					cmd,
					"curl -s -o ./ec2kube.sh https://gitlab.com/freddieptf/lolwhat/raw/master/configuration/init/ec2_kube_node.sh",
					"chmod +x ./ec2kube.sh", "sudo ./ec2kube.sh")
			}
			err := doWithRetry(func() error { return runCmdOnRemoteHost(host, port, cmd, config) }, 3)
			if err != nil {
				log.Fatalf("couldn't set up node %s, %s\n", host, err)
			}
		}(node.provider, node.ip, node.sshPort, config)
	}
	wg.Wait()
}

func initMasters(multiCloud bool, nodes []node, config *ssh.ClientConfig) (joinCmd string, err error) {
	for _, node := range nodes {
		if node.nodeType == "master" {
			cmd := fmt.Sprintf("%s && %s && %s",
				"curl -s -o ./master.sh https://gitlab.com/freddieptf/lolwhat/raw/master/configuration/kubernetes/master.sh",
				"chmod +x ./master.sh", "sudo ./master.sh")
			if multiCloud && node.provider == AWS_PROVIDER {
				cmd = fmt.Sprintf("%s -a $(curl http://169.254.169.254/latest/meta-data/public-ipv4)", cmd)
			}
			err = runCmdOnRemoteHost(node.ip, node.sshPort, cmd, config)
			if err != nil {
				log.Fatalf("couldn't set up node %s, %s\n", node.ip, err)
			}
			var cmdBytes []byte
			cmdBytes, err = readFileFromRemoteHost(node.ip, node.sshPort, "/tmp/join_command", config)
			if err != nil {
				log.Printf("couldn't get the join command from node %s, %s\n", node.ip, err)
			} else {
				joinCmd = string(cmdBytes)
			}
			return // first master, ain't no place for HA here boi
		}
	}
	return "", fmt.Errorf("no master nodes")
}

func initWorkers(joinCmd string, nodes []node, config *ssh.ClientConfig) {
	var wg sync.WaitGroup
	for _, node := range nodes {
		if node.nodeType == "worker" {
			wg.Add(1)
			go func(host, port string, config *ssh.ClientConfig) {
				defer wg.Done()
				cmd := fmt.Sprintf("sudo %s > /home/ubuntu/join.txt", joinCmd)
				err := runCmdOnRemoteHost(host, port, cmd, config)
				if err != nil {
					log.Fatalf("couldn't join cluster, node %s, %s\n", host, err)
				}
			}(node.ip, node.sshPort, config)
		}
	}
	wg.Wait()
}

type node struct {
	nodeType string
	provider string
	ip       string
	sshPort  string
}

func bootstrapCluster(privateKeyPath, passphrase string, multiCloud bool, nodes []node) error {
	key, err := readPrivateKey(privateKeyPath, passphrase)
	if err != nil {
		return err
	}
	sshConfig := &ssh.ClientConfig{
		User: "ubuntu", // this should be configurable but for now we use ubuntu
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(*key),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         10 * time.Second,
	}
	initNodes(multiCloud, nodes, sshConfig)
	joinCmd, err := initMasters(multiCloud, nodes, sshConfig)
	if err != nil {
		return err
	}
	initWorkers(joinCmd, nodes, sshConfig)
	return nil
}

func k8sUp(privateKeyPath, passphrase string, multiCloud bool, nodes []node) {
	fmt.Printf("\n\nHAMMER TIME: %+v\n\n", nodes)
	err := bootstrapCluster(privateKeyPath, passphrase, multiCloud, nodes)
	if err != nil {
		log.Fatal(err)
	}
}
