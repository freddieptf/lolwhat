variable "droplet_ids" {
    type = list(string)
}

variable "node_public_ips" {
    type = list(string)
}

variable "has_public_droplets" {
    type = bool
    default = false
}

variable "public_droplet_ids" {
    type = list(string)
}