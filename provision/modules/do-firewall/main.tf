resource "digitalocean_firewall" "cluster-ssh" {
  name        = "cluster-ssh"
  droplet_ids = var.droplet_ids
  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
}
resource "digitalocean_firewall" "clusterwall" {
  name        = "cluster-networking"
  droplet_ids = var.droplet_ids

  inbound_rule {
    protocol         = "tcp"
    port_range       = "1-65535"
    source_addresses = var.node_public_ips
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "udp"
    port_range       = "1-65535"
    source_addresses = var.node_public_ips
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_firewall" "cluster-ping" {
  name        = "ping"
  droplet_ids = var.droplet_ids

  inbound_rule {
    protocol         = "icmp"
    source_addresses = var.node_public_ips
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_firewall" "http" {
  name        = "http-in"
  droplet_ids = var.public_droplet_ids
  count = var.has_public_droplets ? 1 : 0

  inbound_rule {
    protocol         = "tcp"
    port_range       = "80"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
  inbound_rule {
    protocol         = "tcp"
    port_range       = "443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
}
