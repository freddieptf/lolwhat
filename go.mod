module gitlab.com/freddieptf/lolwhat

go 1.13

require (
	github.com/fatih/color v1.9.0
	github.com/hashicorp/hcl v1.0.0
	github.com/hashicorp/hcl/v2 v2.3.0 // indirect
	golang.org/x/crypto v0.0.0-20200109152110-61a87790db17
	golang.org/x/sys v0.0.0-20200107162124-548cf772de50 // indirect
)
