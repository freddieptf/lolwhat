# lolwhat

ok so first of all...**I don't know what i'm doing here**

***but if you still want to try this then***..this is my attempt at automating bootstrapping a k8s cluster with kubeadm on DO and AWS. EC2, droplets etc are just VMs vibing in different clouds so yeah...

Here's an example config file (later converted to terraform files you can inspect)

```
provider_defaults "aws" {
    region = "eu-west-2"
    vm_image = "ami-00622b440d92e55c0"
    vm_size = "t2.medium"
}

provider_defaults "digitalocean" {
    region = "ams3"
    vm_image = "ubuntu-18-04-x64"
    vm_size = "s-1vcpu-2gb" 
}

node "master" {
  provider = "digitalocean"
  role = "master"
}

node "worker" {
  provider = "aws"
  role = "worker"
}

node "worker1" {
  provider = "digitalocean"
  role = "worker"
}
```
You'll need to set env vars DIGITALOCEAN_TOKEN, AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY to continue

now just run `lolwhat apply --private-key=/path/to/privatekey --key-pass=pass --k8s --k8s-multiCloud --auto-approve` and it should be up and running in < 5mins. Don't ask me about egress costs..or latency..also don't use this thanks

