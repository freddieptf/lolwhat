package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
)

const (
	defaultOutDir     = "./provision"
	defaultConfigFile = "./config.hcl"

	outDirHelp               = "directory to output terraform files to"
	defaultConfigFileHelp    = "hcl file to read config from"
	autoApproveHelp          = "skip terraform approval of plan before applying"
	privateKeyPathHelp       = "path to your ssh private key"
	privateKeyPassphraseHelp = "passphrase for your ssh private key"
	k8sHelp                  = "bootstrap a k8s cluster"
	k8sMultiCloudHelp        = "[Experimental] mad man wants nodes living in different societies"
)

func main() {
	applyFlagSet := flag.NewFlagSet("apply", flag.ExitOnError)
	outDir := applyFlagSet.String("out", defaultOutDir, outDirHelp)
	configPath := applyFlagSet.String("config", defaultConfigFile, defaultConfigFileHelp)
	autoApprove := applyFlagSet.Bool("auto-approve", false, autoApproveHelp)
	privateKeyPath := applyFlagSet.String("private-key", fmt.Sprintf("%s/.ssh/id_rsa", os.Getenv("HOME")), privateKeyPathHelp)
	privateKeyPassphrase := applyFlagSet.String("key-pass", "", privateKeyPassphraseHelp)
	k8s := applyFlagSet.Bool("k8s", false, k8sHelp)
	k8sMultiCloud := applyFlagSet.Bool("k8s-multiCloud", false, k8sMultiCloudHelp)

	planFlagSet := flag.NewFlagSet("plan", flag.ExitOnError)
	planOutDir := planFlagSet.String("out", defaultOutDir, outDirHelp)
	planconfigPath := planFlagSet.String("config", defaultConfigFile, defaultConfigFileHelp)

	args := os.Args
	if len(args) <= 1 {
		applyFlagSet.Usage()
		planFlagSet.Usage()
		os.Exit(1)
	}

	if args[1] == "apply" {
		if err := applyFlagSet.Parse(args[2:]); err != nil {
			log.Fatal(err)
		}

		if err := os.MkdirAll(*outDir, 0777); err != nil {
			log.Fatal(err)
		}

		err := genHCL(*configPath, *outDir)
		if err != nil {
			log.Fatal(err)
		}

		sgChan := make(chan os.Signal, 1)
		signal.Notify(sgChan, syscall.SIGQUIT, syscall.SIGINT)
		ctx, cn := context.WithCancel(context.Background())
		go func() {
			<-sgChan
			cn()
		}()

		hosts, err := terraform(ctx, *outDir, *autoApprove)
		if err != nil {
			log.Fatal(err)
		}

		if *k8s {
			k8sUp(*privateKeyPath, *privateKeyPassphrase, *k8sMultiCloud, *hosts)
		}
	} else if args[1] == "plan" {
		if err := planFlagSet.Parse(args[2:]); err != nil {
			log.Fatal(err)
		}
		if err := os.MkdirAll(*planOutDir, 0777); err != nil {
			log.Fatal(err)
		}

		err := genHCL(*planconfigPath, *planOutDir)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		applyFlagSet.Usage()
		planFlagSet.Usage()
	}
}
