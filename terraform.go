package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"sort"
	"strings"
	"syscall"
)

type output struct {
	Value []string `json:"value"`
}

func getNodesFromTerraformOutPut(outputJson []byte) (*[]node, error) {
	outputMap := make(map[string]output)
	err := json.Unmarshal(outputJson, &outputMap)
	if err != nil {
		return nil, fmt.Errorf("couldn't parse terraform output, %s", err)
	}

	hosts := make(map[string][]string)
	doHosts := outputMap[DIGITAL_OCEAN_PROVIDER+"_hosts"].Value
	sort.Strings(doHosts)
	awsHosts := outputMap[AWS_PROVIDER+"_hosts"].Value
	sort.Strings(awsHosts)
	for k, v := range outputMap {
		if strings.Contains(k, "master") {
			hosts["master"] = append(hosts["master"], v.Value...)
		} else if strings.Contains(k, "worker") {
			hosts["worker"] = append(hosts["worker"], v.Value...)
		} else {
			log.Printf("output value %s, %q\n", k, v)
		}
	}
	nodes := []node{}
	for nodeType, ips := range hosts {
		for _, ip := range ips {
			node := node{nodeType: nodeType, ip: ip, sshPort: "22"}
			if idx := sort.SearchStrings(doHosts, ip); idx < len(doHosts) && doHosts[idx] == ip {
				node.provider = DIGITAL_OCEAN_PROVIDER
			}
			if idx := sort.SearchStrings(awsHosts, ip); idx < len(awsHosts) && awsHosts[idx] == ip {
				node.provider = AWS_PROVIDER
			}
			nodes = append(nodes, node)
		}
	}

	return &nodes, nil
}

func terraform(ctx context.Context, outdir string, autoApprove bool) (*[]node, error) {
	_, err := exec.LookPath("terraform")
	if err != nil {
		return nil, err
	}

	cmd := fmt.Sprintf("cd %s && terraform init && terraform apply -input=false", outdir)
	if autoApprove {
		cmd = cmd + " -auto-approve"
	}
	terraformPlanCmd := exec.Command("sh", "-c", cmd)
	terraformPlanCmd.Stdout = os.Stdout
	terraformPlanCmd.Stderr = os.Stderr
	terraformPlanCmd.Stdin = os.Stdin

	doneChan := make(chan error)
	go func() {
		doneChan <- terraformPlanCmd.Run()

	}()

	select {
	case <-ctx.Done():
		err := terraformPlanCmd.Process.Signal(syscall.SIGINT)
		if err != nil {
			log.Printf("could not signal process PID: %d, err %v", terraformPlanCmd.Process.Pid, err)
		}
		err = <-doneChan
		if err != nil {
			log.Printf("terraform apply err, %v", err)
		}
		return nil, ctx.Err()
	case err := <-doneChan:
		if err != nil {
			return nil, fmt.Errorf("couldn't generate the terraform plan, %v", err)
		}
	}

	outputCmd := exec.CommandContext(ctx, "sh", "-c", fmt.Sprintf("cd %s && terraform output -json", outdir))
	rawJsonOut, err := outputCmd.Output()
	if err != nil {
		return nil, fmt.Errorf("couldn't get terraform output vars, %v", err)
	}

	return getNodesFromTerraformOutPut(rawJsonOut)
}
